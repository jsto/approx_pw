%% init

clear all
close all

%% define surface grid

Nth = 31;
Nv = 30;
th_v = linspace(-pi,pi,Nth);
ve_v = linspace(-3,3,Nv); 

samp_des_idx = fullfact([Nth Nv]);
samp_des = [th_v(samp_des_idx(:,1))', ve_v(samp_des_idx(:,2))'];
clear samp_des_idx

%[th_m,ve_m]=ndgrid(th_v,ve_v);
F_z = @(x,v) sin(x).*v;
F_dz_dv = @(x,v) sin(x);
F_dz_dx = @(x,v) v.*cos(x);

z = F_z(samp_des(:,1),samp_des(:,2));
plot3(samp_des(:,1),samp_des(:,2),z,'o');

plot3(samp_des(:,1),samp_des(:,2),F_dz_dv(samp_des(:,1),samp_des(:,2)),'x')
plot3(samp_des(:,1),samp_des(:,2),F_dz_dx(samp_des(:,1),samp_des(:,2)),'x')

%% construct augmented fit matrix

M_a = mat2cell(samp_des,ones(1,size(samp_des,1)),[2]);
M_a = blkdiag(M_a{:});

%% pls
% t_rms = 1e-6;
% [~,P_full,~,Q_full,B_full,W_full] = pls(M_a,z,t_rms);

[~,~,Mpls.XS,~,~,Mpls.PCTVAR,Mpls.PLSMSE,Mpls.PLSstats] = plsregress(M_a,z,size(M_a,1)-1);
figure
subplot 211
plot(cumsum(Mpls.PCTVAR(2,:)),'-bo');
subplot 212
semilogy(Mpls.PCTVAR(2,:))
%% pls reduce test
Nord = 2;
Mpls.B_red =(Mpls.XS(:,1:Nord)\z);
Mpls.B_real = Mpls.PLSstats.W(:,1:Nord)*Mpls.B_red;
zp = M_a*Mpls.B_real;

figure
plot3(samp_des(:,1),samp_des(:,2),z,'bo', samp_des(:,1),samp_des(:,2),zp,'r.')
rms(z-zp)