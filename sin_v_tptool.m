%% define toolboxes

cd  tptool
addpath([pwd,'/array'], [pwd,'/hull'], [pwd,'/lmi'],[pwd,'/lpv'],[pwd,'/util'])
cd ..

%% init

clear all
close all

%% LPV 

LPV = {@(x) x(2)*sin(x(1));... 
       @(x) x(2)*cos(x(1))...
      };

%% define surface grid

Nth = 51;
Nv = 50;
th_v = linspace(-pi,pi,Nth);
ve_v = linspace(-3,3,Nv); 

[th_m, ve_m] = ndgrid(th_v,ve_v);
samp_des = [th_m(:), ve_m(:)];
% samp_des_idx = fullfact([Nth Nv]);
% samp_des = [th_v(samp_des_idx(:,1))', ve_v(samp_des_idx(:,2))'];
% clear samp_des_idx

y_m = ve_m.*sin(th_m);
x_m = ve_m.*cos(th_m);
%surf(y_m)

%[th_m,ve_m]=ndgrid(th_v,ve_v);
F = @(m) cellfun( @(x) [x(2)*sin(x(1)),x(2)*cos(x(1))],...
                  mat2cell(m,ones(size(m,1),1)),'UniformOutput',false);
F_J = @(m) cellfun(@(x) [x(2)*cos(x(1)),sin(x(1)); -x(2)*sin(x(1)),cos(x(1))],...
                    mat2cell(m,ones(size(m,1),1)),'UniformOutput',false);

%z = cell2mat(F(samp_des));
z = [y_m(:), x_m(:)];
plot3(samp_des(:,1),samp_des(:,2),z,'o');

zJ = F_J(samp_des);
zJ = cat(3,zJ{:});

zJ_t4 = shiftdim(cat(4,cat(3,th_m.*cos(ve_m),sin(ve_m)),cat(3, -th_m.*sin(ve_m),cos(ve_m))),2);

plot3(samp_des(:,1),samp_des(:,2),reshape(zJ,4,[]),'x');
%% approximate with tensor
U = cpd(zJ,4);
U4 = cpd(zJ_t4,4)
%rankest(zJ_t4)
%frob(cpdres(zJ_t4,U4))/frob(zJ_t4)

% new coordinates
xx = U{2}'*samp_des';

plot(xx(1,:),U{3}(:,1),'.')