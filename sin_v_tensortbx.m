%% define toolboxes

cd  'poblano_toolbox_1.1'
addpath(pwd)
cd ../

cd 'tensor_toolbox_2.5'
addpath(pwd) %<-- Add the tensor toolbox to the MATLAB path
cd met; addpath(pwd)  %<-- [OPTIONAL] Also add the met directory
cd ../../

%% init

clear all
close all

%% define surface grid

Nth = 51;
Nv = 50;
th_v = linspace(-pi,pi,Nth);
ve_v = linspace(-3,3,Nv); 

[th_m, ve_m] = ndgrid(th_v,ve_v);
samp_des = [th_m(:), ve_m(:)];
% samp_des_idx = fullfact([Nth Nv]);
% samp_des = [th_v(samp_des_idx(:,1))', ve_v(samp_des_idx(:,2))'];
% clear samp_des_idx

y_m = ve_m.*sin(th_m);
x_m = ve_m.*cos(th_m);
%surf(y_m)

%[th_m,ve_m]=ndgrid(th_v,ve_v);
F = @(m) cellfun( @(x) [x(2)*sin(x(1)),x(2)*cos(x(1))],...
                  mat2cell(m,ones(size(m,1),1)),'UniformOutput',false);
F_J = @(m) cellfun(@(x) [x(2)*cos(x(1)),sin(x(1)); -x(2)*sin(x(1)),cos(x(1))],...
                    mat2cell(m,ones(size(m,1),1)),'UniformOutput',false);

%z = cell2mat(F(samp_des));
z = [y_m(:), x_m(:)];
plot3(samp_des(:,1),samp_des(:,2),z,'o');

zJ = F_J(samp_des);
zJ = cat(3,zJ{:});

zJ_t4 = shiftdim(cat(4,cat(3,th_m.*cos(ve_m),sin(ve_m)),cat(3, -th_m.*sin(ve_m),cos(ve_m))),2);

plot3(samp_des(:,1),samp_des(:,2),reshape(zJ,4,[]),'x');

%% approximate with tensor
zJ_T = tensor(zJ);
zJ_T4 = tensor(zJ_t4);

Ucp = cp_als(zJ_T,4);
Ucp4 = cp_als(zJ_T4,4)
Ut = tucker_als(zJ_T,2);

% new coordinates
xx = Ucp.u{2}'*samp_des';

plot3(xx(1,:),xx(2,:),Ucp.u{3}(:,1),'.')