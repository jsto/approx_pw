%% define toolboxes

cd  poblano_toolbox_1.1
addpath(pwd)

cd tensor_toolbox_2.5
addpath(pwd) %<-- Add the tensor toolbox to the MATLAB path
cd ../met; addpath(pwd)  %<-- [OPTIONAL] Also add the met directory

%% init

clear all
close all

%% define surface grid

Nth = 31;
Nv = 30;
th_v = linspace(-pi,pi,Nth);
ve_v = linspace(-3,3,Nv); 

samp_des_idx = fullfact([Nth Nv]);
samp_des = [th_v(samp_des_idx(:,1))', ve_v(samp_des_idx(:,2))'];
clear samp_des_idx

%[th_m,ve_m]=ndgrid(th_v,ve_v);
F_z = @(x) sin(x(:,1)).*x(:,2);
F_dz_dv = @(x) sin(x(:,1));
F_dz_dx = @(x) x(:,2).*cos(x(:,1));

z = F_z(samp_des);
plot3(samp_des(:,1),samp_des(:,2),z,'o');

plot3(samp_des(:,1),samp_des(:,2),F_dz_dv(samp_des),'x')
plot3(samp_des(:,1),samp_des(:,2),F_dz_dx(samp_des),'x')

%% construct gradients
clear dz_dvdx
dz_dvdx(1,:,:) = [F_dz_dx(samp_des),F_dz_dv(samp_des)]';

[S U sv tol] = hosvd(dz_dvdx);

SS=(shiftdim(S)*U{3}')';

plot(SS)
plot(shiftdim(dz_dvdx,1)')

%% simple svd
[Us,Ss,Vs] = svd(squeeze(dz_dvdx)',0);
newv = Vs*samp_des';
Tt = (Vs^-1)*(Us*Ss)';
plot(newv(2,:),Tt(2,:),'.')